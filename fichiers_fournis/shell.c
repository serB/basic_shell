#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "readcmd.h"


void redirect_output(int fd){
  if (fd != STDOUT_FILENO) {
        dup2(fd, STDOUT_FILENO);
        close(fd);
    }
}

void redirect_input(int fd){
    if (fd != STDIN_FILENO) {
        dup2(fd, STDIN_FILENO);
        close(fd);
    }
}

int main(){

    struct cmdline *l;
    int i,pid;
    int fd_in,fd_out;
    int pipefd[2];

    while (1) {
        fd_in = STDIN_FILENO;
        fd_out = STDOUT_FILENO;
        printf("shell> ");
        l = readcmd();

        if (l==NULL) {       // input stream is closed
            printf("exit\n");
            exit(0);
        }

        if (l->err) {           // display the error message
            printf("error: %s\n", l->err);
        }

        if (l->in){
            fd_in = open(l->in,O_RDONLY);
            if (fd_in < 0) {
                printf("error: incorrect input\n");
            }
        }

        if (l->out){
            fd_out = open(l->out,O_WRONLY|O_TRUNC|O_CREAT,00700);
            if (fd_out < 0) {
                printf("error: incorrect output\n");
            }
        }


        /* Exec the command */
        for (i=0; l->seq[i]!=0; i++) {
            pipe(pipefd);
            pid = fork();
            if(pid==-1){exit(1);}

            else if(pid==0){                // child process
                if(l->seq[i+1] == NULL){                                        // if last command

                    close(pipefd[0]);   // We close the pipe as it is now useless
                    close(pipefd[1]);

                    redirect_input(fd_in);
                    redirect_output(fd_out);

                    execvp(l->seq[i][0],l->seq[i]);
                }
                else{                                                                       // if not last command
                    close(fd_out);
                    close(pipefd[0]);

                    redirect_input(fd_in);
                    redirect_output(pipefd[1]);

                   execvp(l->seq[i][0],l->seq[i]);
                }
                return 0;
            }

            else{                                   // parent process
                waitpid(pid,NULL,0);

                // closing the fd
                close(pipefd[1]);
                if (fd_in != STDIN_FILENO){close(fd_in);}
                fd_in =pipefd[0];
                }
        }
	}
}
